<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>view Contacts</title>
<style>
#customers {
  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#customers td, #customers th {
  border: 1px solid #ddd;
  padding: 8px;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}

#customers th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #4CAF50;
  color: white;
}
</style>
</head>
<body>
<h1>my contacts list</h1>
<table border=1 id="customers">
<tr>
	<td>Name</td>
	<td>Email</td>
	<td>Country</td>
</tr>
	<c:forEach items="${contacts_m}" var="contact">
		<tr>
			<td>${contact.name}</td>
			<td>${contact.email}</td>
			<td>${contact.country}</td>
		</tr>		 
	</c:forEach>

</table>
<a href="/add_contact">click here to add new contact....</a>
</body>
</html>