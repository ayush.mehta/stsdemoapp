package com.webapp.demo;

import java.util.ArrayList;


public class ContactRepo {
	static ArrayList<ContactModel> list=new ArrayList<ContactModel>();
	public void initList() {
		list.add(new ContactModel("Ayush","ayush@gmail.com","India"));
		list.add(new ContactModel("Nikhil","nikhil@gmail.com","India"));
		list.add(new ContactModel("Chandan","chandan@gmail.com","India"));
	}
	public ArrayList<ContactModel> getContactList(){
		
		
		
		return list;
	}
	public void addContact(ContactModel mv) {
		list.add(mv);
		
	}
}
