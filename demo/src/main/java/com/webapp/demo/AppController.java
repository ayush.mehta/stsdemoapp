package com.webapp.demo;

import java.util.ArrayList;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class AppController {
	@RequestMapping("/")
	public String welcome() {
		ContactRepo cr=new ContactRepo();
		cr.initList();
		return "index";
	}
	
	
	@RequestMapping("/list_contact")
	public String listContact(Model model) {
		ContactRepo cr=new ContactRepo();
		ArrayList<ContactModel> list= cr.getContactList();
		model.addAttribute("contacts_m",list);
		return "contacts";
	}
	
	@RequestMapping("/add_contact")
	public String addContact(ContactModel mv) {
		ContactRepo cr=new ContactRepo();
		cr.addContact(mv);
		return "addcontact";
	}
}
